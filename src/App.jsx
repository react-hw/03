import React, { useState, useEffect } from "react";
import { Routes, Route } from "react-router-dom";

import sendRequest from "./helpers/sendRequest.js";
import "./App.scss";

import Header from "./components/Header";
import SectionPruducts from "./components/Section";
import ModalAddProduct, {
  ModalDelCart,
  ModalDelFavorite,
} from "./components/Modal";
import Footer from "./components/Footer/Footer.jsx";

import CartPage from "./pages/Cartpage.jsx";
import FavoritePage from "./pages/Favoritepage.jsx";
import NotFoundPage from "./pages/Notfoundpage.jsx";

const App = () => {
  const [isModalProduct, setIsModalProduct] = useState(false);
  const [isModalDelCart, setIsModalDelCart] = useState(false);
  const [isModalDelFavorite, setIsModalDelFavorite] = useState(false);

  const [products, setProducts] = useState([]);
  const [currentProduct, setCurrentProduct] = useState({});
  const [carts, setToCart] = useState(
    JSON.parse(localStorage.getItem("carts")) || []
  );
  const [favorites, setFavorite] = useState(
    JSON.parse(localStorage.getItem("favorites")) || []
  );

  useEffect(() => {
    const savedProducts = JSON.parse(localStorage.getItem("products"));
    if (savedProducts) {
      setProducts(savedProducts);
    } else {
      sendRequest("products.json").then((data) => {
        setProducts(
          data.map((product) => ({
            ...product,
            addedToCart: false,
            addedToFavorite: false,
            basketCounter: 0,
          }))
        );
      });
    }
  }, []);

  const handleToCart = (item) => {
    setProducts((prevState) => {
      const updatedProducts = prevState.map((product) =>
        product.vendorCode === item.vendorCode
          ? {
              ...product,
              addedToCart: true,
              basketCounter: product.basketCounter + 1,
            }
          : product
      );
      localStorage.setItem("products", JSON.stringify(updatedProducts));
      return updatedProducts;
    });

    const existingProductIndex = carts.findIndex(
      (p) => p.vendorCode === item.vendorCode
    );

    let newState;
    if (existingProductIndex >= 0) {
      newState = carts.map((p, index) =>
        index === existingProductIndex
          ? { ...p, addedToCart: true, basketCounter: p.basketCounter + 1 }
          : p
      );
    } else {
      newState = [...carts, { ...item, addedToCart: true, basketCounter: 1 }];
    }
    setToCart(newState);
    localStorage.setItem("carts", JSON.stringify(newState));
    localStorage.setItem("favorites", JSON.stringify(newState));
  };

  const removeCart = (item) => {
    const updatedProducts = products.map((product) =>
      product.vendorCode === item.vendorCode
        ? { ...product, addedToCart: false, basketCounter: 0 }
        : product
    );
    setProducts(updatedProducts);
    localStorage.setItem("products", JSON.stringify(updatedProducts));

    const updatedCarts = carts.filter(
      (cartItem) => cartItem.vendorCode !== item.vendorCode
    );
    setToCart(updatedCarts);
    localStorage.setItem("carts", JSON.stringify(updatedCarts));

    const updatedFavorites = favorites.map((favoriteItem) =>
      favoriteItem.vendorCode === item.vendorCode
        ? { ...favoriteItem, addedToCart: false, basketCounter: 0 }
        : favoriteItem
    );
    setFavorite(updatedFavorites);
    localStorage.setItem("favorites", JSON.stringify(updatedFavorites));
  };

  const handleFavorite = (item) => {
    const updatedProducts = products.map((product) =>
      product.vendorCode === item.vendorCode
        ? { ...product, addedToFavorite: !item.addedToFavorite }
        : product
    );
    setProducts(updatedProducts);
    localStorage.setItem("products", JSON.stringify(updatedProducts));

    const updatedCarts = carts.map((cartItem) =>
      cartItem.vendorCode === item.vendorCode
        ? { ...cartItem, addedToFavorite: !item.addedToFavorite }
        : cartItem
    );
    setToCart(updatedCarts);
    localStorage.setItem("carts", JSON.stringify(updatedCarts));

    if (item.addedToFavorite) {
      const updatedFavorites = favorites.filter(
        (fav) => fav.vendorCode !== item.vendorCode
      );
      setFavorite(updatedFavorites);
      localStorage.setItem("favorites", JSON.stringify(updatedFavorites));
    } else {
      setFavorite((prevState) => [...prevState, item]);
      localStorage.setItem("favorites", JSON.stringify([...favorites, item]));
    }
  };

  const removeFavorite = (item) => {
    const updatedProducts = products.map((product) =>
      product.vendorCode === item.vendorCode
        ? { ...product, addedToFavorite: false }
        : product
    );
    setProducts(updatedProducts);
    localStorage.setItem("products", JSON.stringify(updatedProducts));

    const updatedFavorites = favorites.filter(
      (fav) => fav.vendorCode !== item.vendorCode
    );
    setFavorite(updatedFavorites);
    localStorage.setItem("favorites", JSON.stringify(updatedFavorites));

    const updatedCarts = carts.map((cartItem) =>
      cartItem.vendorCode === item.vendorCode
        ? { ...cartItem, addedToFavorite: false }
        : cartItem
    );
    setToCart(updatedCarts);
    localStorage.setItem("carts", JSON.stringify(updatedCarts));
  };

  const handlCurrentProduct = (product) => {
    setCurrentProduct(product);
  };

  const handleModalProduct = () => {
    setIsModalProduct((prevState) => !prevState);
  };

  const handleModalDelCart = () => {
    setIsModalDelCart((prevState) => !prevState);
  };

  const handleModalDelFavorite = () => {
    setIsModalDelFavorite((prevState) => !prevState);
  };

  return (
    <div className="container">
      <Header countCarts={carts.length} countFavorites={favorites.length} />

      <main className="inner main">
        <Routes>
          <Route
            path="/"
            element={
              <SectionPruducts
                handleModalProduct={handleModalProduct}
                onCurrentProduct={handlCurrentProduct}
                products={products}
                handleFavorite={handleFavorite}
              />
            }
          />
          <Route
            path="/carts"
            element={
              <CartPage
                carts={carts}
                handleModalDelCart={handleModalDelCart}
                onCurrentProduct={handlCurrentProduct}
                handleFavorite={handleFavorite}
              />
            }
          />
          <Route
            path="/favorites"
            element={
              <FavoritePage
                isOpenModalPruduct={handleModalProduct}
                handleModalDelFavorite={handleModalDelFavorite}
                onCurrentProduct={handlCurrentProduct}
              />
            }
          />
          <Route path="*" element={<NotFoundPage />} />
        </Routes>
      </main>

      <Footer className="footer">React HomeWork | Kucher Andrii | 2024</Footer>

      <ModalAddProduct
        isOpen={isModalProduct}
        onClose={handleModalProduct}
        currentProduct={currentProduct}
        handleModalProduct={handleModalProduct}
        onAddToCart={() => {
          handleToCart(currentProduct);
        }}
      />

      <ModalDelCart
        isOpen={isModalDelCart}
        onClose={handleModalDelCart}
        currentProduct={currentProduct}
        removeCart={removeCart}
      />

      <ModalDelFavorite
        isOpen={isModalDelFavorite}
        onClose={handleModalDelFavorite}
        currentProduct={currentProduct}
        removeFavorite={removeFavorite}
      />
    </div>
  );
};

export default App;
