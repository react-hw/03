import React from "react";
import PropTypes from "prop-types";

import ModalWrapper, {
  ModalBox,
  ModalContainer,
  ModalHeader,
  ModalMain,
  ModalFooter,
} from "./components";

import ButtonCornerClose from "../Button/ButtonCornerClose";
import Button from "../Button";

const ModalDelCart = (props) => {
  const { isOpen, onClose, removeCart, currentProduct } = props;
  const { name, path } = currentProduct;

  const handleRemoveCart = () => {
    removeCart(currentProduct);
    onClose();
  };

  return (
    <ModalWrapper isOpen={isOpen} onClick={onClose}>
      <ModalBox>
        <ButtonCornerClose onClick={onClose} />
        <ModalContainer>
          <ModalHeader>
            <div className="modal__header-img-box">
              <img src={path} alt={name} className="modal__header-img" />
            </div>
          </ModalHeader>

          <ModalMain currentProduct={currentProduct}>
            Delete <span>{name}</span> from cart
          </ModalMain>

          <ModalFooter>
            <div className="modal__footer-wrapper">
              <Button className="focus" onClick={onClose}>
                NO, CANCEL
              </Button>
              <Button onClick={handleRemoveCart}>YES, DELETE</Button>
            </div>
          </ModalFooter>
        </ModalContainer>
      </ModalBox>
    </ModalWrapper>
  );
};

ModalDelCart.propTypes = {
  isOpen: PropTypes.bool,
  onClose: PropTypes.func,
  removeCart: PropTypes.func,
  currentProduct: PropTypes.object,
};

export default ModalDelCart;
