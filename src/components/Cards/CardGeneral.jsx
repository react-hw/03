import React, { useState } from "react";
import PropTypes from "prop-types";
import cn from "classnames";

import "./Cards.scss";

import { ButtonCornerCart, ButtonCornerHeart } from "../Button";
import CardContent from "./";

const CardGeneral = (props) => {
  const { className, product, onModalProduct, handleFavorite } = props;
  const [isFavorite, setIsFavorite] = useState(product.addedToFavorite);
  const counterBasket = product.basketCounter;

  const toggleFavorite = () => {
    handleFavorite(product);
    setIsFavorite(!isFavorite);
  };

  return (
    <div className={cn("card", className)}>
      <ButtonCornerCart
        onClick={onModalProduct}
        isActive={product.addedToCart}
        counterBasket={counterBasket}
      />
      <ButtonCornerHeart onClick={toggleFavorite} isActive={isFavorite} />

      <CardContent product={product} />
    </div>
  );
};

CardGeneral.propTypes = {
  className: PropTypes.string,
  product: PropTypes.object,
  onModalProduct: PropTypes.func,
  handleFavorite: PropTypes.func,
};

export default CardGeneral;
