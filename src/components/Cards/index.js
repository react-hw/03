// export { default } from "./Card";

import CardContent from "./CardContent.jsx";
import CardGeneral from "./CardGeneral.jsx";
import CardToCart from "./CardToCart.jsx";
import CardFavorite from "./CardFavorite.jsx";

export default CardContent;
export { CardGeneral, CardToCart, CardFavorite };
