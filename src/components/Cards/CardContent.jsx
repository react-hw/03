import React from "react";
import PropTypes from "prop-types";
import cn from "classnames";

import "./Cards.scss";

const CardContent = ({ className, product }) => {
  const { name, price, color, vendorCode, path } = product;
  return (
    <div className={cn("card__content", className)}>
      <header className={cn("card__header", className)}>{name}</header>
      <main className={cn("card__main", className)}>
        <img src={path} alt={name} className="card__main-img" />
      </main>
      <footer className={cn("card__footer", className)}>
        <p>Price: ${price}</p>
        <p>Color: {color}</p>
        <p>Article: {vendorCode}</p>
      </footer>
    </div>
  );
};

CardContent.propTypes = {
  className: PropTypes.string,
  product: PropTypes.object,
};

export default CardContent;
