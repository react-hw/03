import React from "react";
import PropTypes from "prop-types";
import cn from "classnames";

import "./Section.scss";

const SectionWrap = ({ className, children }) => {
  return <div className={cn("section-wrap", className)}>{children}</div>;
};

SectionWrap.propTypes = {
  className: PropTypes.string,
  children: PropTypes.any,
};

export default SectionWrap;
