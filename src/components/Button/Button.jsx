import React from "react";
import PropTypes from "prop-types";
import cn from "classnames";

import "./Button.scss";

const Button = (props) => {
  const { className, type, onClick, children, ...restProps } = props;

  return (
    <button
      className={cn("btn", className)}
      type={type}
      onClick={onClick}
      {...restProps}
    >
      {children}
    </button>
  );
};

Button.defaultProps = {
  type: "button",
};

Button.propTypes = {
  children: PropTypes.any,
  className: PropTypes.string,
  type: PropTypes.string,
  onClick: PropTypes.func,
  restProps: PropTypes.object,
};

export default Button;
