import React from "react";
import PropTypes from "prop-types";
import cn from "classnames";

import "./ButtonPlate.scss";

const ButtonPlate = (props) => {
  const { className, type, onClick, children, ...restProps } = props;
  return (
    <button
      className={className}
      type={type}
      onClick={onClick}
      {...restProps}
    >
      {children}
    </button>
  );
};

ButtonPlate.defaultProps = {
  type: "button",
};

ButtonPlate.propTypes = {
  className: PropTypes.string,
  type: PropTypes.string,
  onClick: PropTypes.func,
  children: PropTypes.any,
  restProps: PropTypes.object,
};

export default ButtonPlate;
