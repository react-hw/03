import React from "react";
import PropTypes from "prop-types";
import cn from "classnames";

import "./ButtonCornerCart.scss";

import { ButtonCorner } from "./";
import { Cart } from "../../assets/index.js";

const ButtonCornerCart = ({ onClick, isActive, counterBasket }) => {
  const counterBasketDecades = counterBasket > 0;

  return (
    <ButtonCorner className="btn__corner-left" onClick={onClick}>
      <div className="btn__corner--wrap">
        <Cart className={cn("svg", "svg_cart", { active: isActive })} />
        {counterBasketDecades && (
          <p className="counter__corner-btn">{counterBasket}</p>
        )}
      </div>
    </ButtonCorner>
  );
};

ButtonCornerCart.propTypes = {
  onClick: PropTypes.func,
  isActive: PropTypes.bool,
  counterBasket: PropTypes.number,
};

export default ButtonCornerCart;
