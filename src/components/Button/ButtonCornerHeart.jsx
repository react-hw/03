import React from "react";
import PropTypes from "prop-types";
import cn from "classnames";

import { ButtonCorner } from "./";
import { Heart } from "../../assets";

const ButtonCornerHeart = ({ className, onClick, isActive }) => {
  return (
    <ButtonCorner className={cn(className)} onClick={onClick}>
      <Heart className={cn("svg", "svg--heart", { active: isActive })} />
    </ButtonCorner>
  );
};

ButtonCornerHeart.propTypes = {
  className: PropTypes.string,
  onClick: PropTypes.func,
  isActive: PropTypes.bool,
};

export default ButtonCornerHeart;
