import React from "react";
import PropTypes from "prop-types";

import Section, { SectionWrap } from "../components/Section/components";
import { CardToCart } from "../components/Cards";
const CartPage = ({
  carts,
  handleModalDelCart,
  onCurrentProduct,
  handleFavorite,
}) => {
  return (
    <Section>
      <SectionWrap>
        {carts.map((product) => {
          return (
            <CardToCart
              key={product.vendorCode}
              product={product}
              onModalDelCart={() => {
                handleModalDelCart();
                onCurrentProduct(product);
              }}
              handleFavorite={handleFavorite}
            />
          );
        })}
      </SectionWrap>
    </Section>
  );
};

CartPage.propTypes = {
  carts: PropTypes.array,
  onCurrentProduct: PropTypes.func,
  handleModalDelCart: PropTypes.func,
  handleFavorite: PropTypes.func,
};

export default CartPage;
