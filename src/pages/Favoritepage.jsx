import React from "react";
import PropTypes from "prop-types";

import Section, { SectionWrap } from "../components/Section/components";
import { CardFavorite } from "../components/Cards";

const FavoritePage = (props) => {
  const { onCurrentProduct, isOpenModalPruduct, handleModalDelFavorite } =
    props;

  const localFavorites = JSON.parse(localStorage.getItem("favorites")) || [];
  return (
    <Section>
      <SectionWrap>
        {localFavorites.map((product) => {
          return (
            <CardFavorite
              key={product.vendorCode}
              product={product}
              onModalProduct={() => {
                isOpenModalPruduct();
                onCurrentProduct(product);
              }}
              onModalDelFavorite={() => {
                handleModalDelFavorite();
                onCurrentProduct(product);
              }}
            />
          );
        })}
      </SectionWrap>
    </Section>
  );
};

FavoritePage.propTypes = {
  onCurrentProduct: PropTypes.func,
  isOpenModalPruduct: PropTypes.func,
  handleModalDelFavorite: PropTypes.func,
};

export default FavoritePage;
